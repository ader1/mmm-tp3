package com.istic.ader.map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

/**
 * Created by ader on 08/11/15.
 */
public class WebViewPortrait extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebViewFragment webvf = new WebViewFragment();

        Intent i = this.getIntent();
        Bundle bundle = getIntent().getExtras();

     Double la = bundle.getDouble("la");
        Double lo = bundle.getDouble("lo");
       String link = bundle.getString("link");

        webvf.init(link);
        webvf.initCordo(la,lo);
        getFragmentManager().beginTransaction().add(android.R.id.content, webvf).commit();

    }
}
