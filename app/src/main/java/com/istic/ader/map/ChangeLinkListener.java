package com.istic.ader.map;

/**
 * Created by ader on 05/11/15.
 */
public interface ChangeLinkListener {
    public void onLinkChange(String link);
    public void update (Double la, Double lon);
    public void addMapFragment();
}
