package com.istic.ader.map;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by ader on 05/11/15.
 */
public class WebViewFragment extends Fragment {
    private String currentURL;
    Button bouton;
    private Double latitude;
    private Double longitude;
    /* Fonction permettant l'initialisation de l'url */
    public void init(String url) {
        currentURL = url;
    }
    /* Fontion permettant l'initialisation de latitude et longitude*/
    public void initCordo(Double la , Double lo){
        latitude=la;
        longitude=lo;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("SwA", "WVF onCreateView");
        View v = inflater.inflate(R.layout.web_veiw, container, false);
        bouton = (Button) v.findViewById(R.id.btm);
        // Ajout d'un listner sur le bouton localisation
        bouton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Dans le cas de tablet
                if (getResources().getConfiguration().orientation ==  Configuration.ORIENTATION_LANDSCAPE) ((ChangeLinkListener)  getActivity()).addMapFragment();
                // Dans le cas de smartphone
                else {
                    Log.d("test", "Portrait");
                    Log.d("latitude",latitude.toString());
                    Log.d("longitude", longitude.toString());


                    // Creation de l'objet contenant de framgment map
                    MapsActivity m = new MapsActivity();
                    // Initialisation de latitude et longitude
                    m.init(latitude,longitude);
                    // Remplacement de fragment webview par fragment map
                    getFragmentManager().beginTransaction().replace(android.R.id.content, m).commit();
                }
            }
        });
        if (currentURL != null) {

            WebView wv = (WebView) v.findViewById(R.id.webPage);
            wv.getSettings().setJavaScriptEnabled(true);
            wv.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            wv.loadUrl(currentURL);
        }
        return v;
    }

    public void updateUrl(String url) {
        Log.d("SwA", "Update URL ["+url+"] - View ["+getView()+"]");
        currentURL = url;
        WebView wv = (WebView) getView().findViewById(R.id.webPage);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl(url);

    }


}
