package com.istic.ader.map;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ader on 29/10/15.
 */
public class FragmentListveiw extends Fragment{
    private final Map<String,String> myMap1 = new HashMap<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //This layout contains your list view
        View view = inflater.inflate(R.layout.fragment1_main, container, false);
        final ListView listview =(ListView)view.findViewById(R.id.listviewperso);

        myMap1.put("Alsace", "http://technoresto.org/vdf/alsace/index.shtml" );
        myMap1.put("Beaujolais", "http://technoresto.org/vdf/beaujolais/index.shtml");
        myMap1.put("jura", "http://technoresto.org/vdf/jura/index.html");
        myMap1.put("Champagne", "http://technoresto.org/vdf/champagne/index.html");
        myMap1.put("Savoie", "http://technoresto.org/vdf/savoie/index.html");
        myMap1.put("Languedoc-Roussillon", "http://technoresto.org/vdf/languedoc/index.html");
        myMap1.put("Bordelais", "http://technoresto.org/vdf/bordelais/index.html");
        myMap1.put("Vallée du Rhone", "http://technoresto.org/vdf/cotes_du_rhone/index.html");
        myMap1.put("Provence", "http://technoresto.org/vdf/provence/index.html");
        myMap1.put("Val de Loire", "http://technoresto.org/vdf/val_de_loire/index.html");
        myMap1.put("Sud-Ouest", "http://technoresto.org/vdf/sud-ouest/index.html");
        myMap1.put("Corse", "http://technoresto.org/vdf/corse/index.html");
        myMap1.put("Bourgogne", "http://technoresto.org/vdf/bourgogne/index.html");
        final Double[] latitude = {48.31817949999999,46.083333,46.76247499999999,48.026628, 45.4932045,43.5912356,44.4932112,45.070571,46.097248,47.6666667,44.894539,42.0396042,47.0525047} ;
        final Double [] longitude={7.441624100000013,4.666667,5.672915900000021,0.3332350000000588,6.472399999999993,3.258362600000055,-0.23409970000000158,4.769464999999968,4.580979999999954,1.5833333000000493,-0.5723100000000159,9.012892599999986,4.383721499999979};

        String[] items = new String[] {"Alsace", "Beaujolais", "jura","Champagne","Savoie","Languedoc-Roussillon","Bordelais",
                "Vallée du Rhone","Provence","Val de Loire","Sud-Ouest","Corse","Bourgogne"};

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);

        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parentAdapter, View view, int position,
                                    long id) {
                 ((ChangeLinkListener)  getActivity()).update(latitude[(int) adapter.getItemId(position)], longitude[(int) adapter.getItemId(position)]);
                ((ChangeLinkListener)  getActivity()).onLinkChange(myMap1.get(adapter.getItem(position)));
            }

        });
        return view;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }



}
