package com.istic.ader.map;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class MainActivity extends FragmentActivity implements ChangeLinkListener{

    private Double latitude;
    private Double longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public void onLinkChange(String link) {

        /* Ici on detecte si on a un double fragment (tablet)*/
        if (findViewById(R.id.fragPage) != null) {
               // Creation de l'objet(Fragment webview)
                WebViewFragment wvf = new WebViewFragment();
                wvf.init(link);

                // Nous sommmes dans le mode  Tablet
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();


                // Remplacement de Framelayer par le fragment du webview
                ft.replace(R.id.fragPage, wvf);
                ft.commit();
        }

        /*Ici le cas d'un smartphone*/
        else {
            System.out.println("Start Activity");
            Intent mportrait = new Intent(this, WebViewPortrait.class);
            mportrait.putExtra("link", link);
            mportrait.putExtra("la", latitude);
            mportrait.putExtra("lo", longitude);
            startActivity(mportrait);
        }

    }

    /*Fonction permettant de mettre à jours les variables latitude et longitude lorsque on selectionne une région sur la liste view */
    @Override
    public void update(Double la, Double lon) {
        latitude=la;
        longitude=lon;
    }

    /*Fonction permettant de gerer la génération du fragment du map en landscape */
    @Override
    public void addMapFragment(){

        addMapFragmentaux(latitude,longitude);
    }

    /* Fonction auxiliare pour addMapFragment()*/
    private void addMapFragmentaux(Double la , Double lon) {
            //Creation de l'objet qui contient le fragment Map
            MapsActivity map  = new MapsActivity();
            // Initialisation des variables longitude et latitude
            map.init(la,lon);


            // Nous sommes dans le mode tablet
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            // Remplacement du  Framelayer
            ft.replace(R.id.fragPage, map);
            ft.commit();
    }


}
