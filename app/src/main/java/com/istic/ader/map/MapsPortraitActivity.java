package com.istic.ader.map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

/**
 * Created by ader on 13/11/15.
 */
public class MapsPortraitActivity extends FragmentActivity{
    private MapsActivity m ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = this.getIntent();
        Bundle bundle = getIntent().getExtras();

        Double la = bundle.getDouble("la");
        Double lo = bundle.getDouble("lo");
        m = new MapsActivity();
        m.init(la,lo);

        getFragmentManager().beginTransaction().add(android.R.id.content, m).commit();
    }
}
